Module.register("mmm-clear-skys",{
    defaults: {
        img: "PortORcsk.gif",
        update: 1   // In hours
    },
    
    // Define start sequence.
	start: function() {
		Log.info("Starting module: " + this.name);

		// Schedule update interval.
		var self = this;
		setInterval(function() {
            Log.info("Refresh: " + this.name);
			self.updateDom();
		}, 1000*self.config.update*3600);
    },
    
    	// Override dom generator.
	getDom: function() {
		var wrapper = document.createElement("div");
		wrapper.innerHTML = "<img src='https://www.cleardarksky.com/c/" + this.config.img + "'  style='height: 100%; width: 100%; object-fit: contain'>";
        Log.info("Wrapper: " + wrapper.innerHTML);
		return wrapper;
	}
});
