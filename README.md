# Magic Mirror 2 Module: Clear Skys

![example image](https://gitlab.com/croxis/mmm-clear-skys/-/raw/master/example.png)

A very simple module that pulls sky clarity forcasts from cleardarksky.com for astronomical viewing clarity. The data originates from the Canadian Weather services, so locations are only provided for North America.

## Getting the forcast

Forcasts are scraped from https://www.cleardarksky.com/csk/  Find the forcast page for your closest location. Then right click on the forcast image to get the image name. See the config example below.

## Using the module

To use this module, add it to the modules array in the `config/config.js` file:
````javascript
modules: [
	{
		module: 'mmm-clear-skys',
		position: 'top_right',	// This can be any of the regions.
            config: {  
                img: 'LDsScpsNMcsk.gif'  // This is the forcast image https://www.cleardarksky.com/c/LDsScpsNMcsk.gif from the site page https://www.cleardarksky.com/c/LDsScpsNMkey.html
            }
	}
]
````


